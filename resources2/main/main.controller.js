var pop;
sap.ui.define([
		'sap/m/MessageToast',
		'sap/ui/core/Fragment',
		'sap/ui/core/mvc/Controller',
		'sap/m/TabContainerItem',
		'sap/m/MessageBox',
		'sap/m/FlexBox',
		'sap/ui/model/json/JSONModel'
		
	], function(MessageToast, Fragment, Controller,TabContainerItem, MessageBox,FlexBox,JSONModel) {
	"use strict";

	var MainController = Controller.extend("main.main", {

		onInit: function(){
		sap.m.Button.extend("HoverButton", { // call the new Control type "HoverButton" 
														// and let it inherit from sap.ui.commons.Button
			  metadata: {
				  events: {
					  "hover" : {}  // this Button has also a "hover" event, in addition to "press" of the normal Button
				  }
			  },
		  
			  // the hover event handler:
			  onmouseover : function(evt) {   // is called when the Button is hovered - no event registration required
				  this.fireHover();
			  },

			  renderer: {} // add nothing, just inherit the ButtonRenderer as is; 
						   // In this case (since the renderer is not changed) you could also specify this explicitly with:  renderer:"sap.ui.commons.ButtonRenderer"
						   // (means you reuse the ButtonRenderer instead of creating a new view
		  });
			//first.addStyleClass("grey");
			var data = {
					now : new Date(),
aMockMessages : [{
				type: 'Warning',
				title: 'Information message',
				description: 'First Information message description',
				subtitle: 'Example of subtitle',
				counter: 1
			}]					
			};
    
			var oModel = new JSONModel();
			oModel.setData(data);
			this.getView().setModel(oModel);		
			var that = this;

			var oMessageTemplate = new sap.m.MessageItem({
				type: '{type}',
				title: '{title}',
				description: '{description}',
				subtitle: '{subtitle}',
				counter: '{counter}',
				markupDescription: "{markupDescription}"
			});


				that = this;
			

			var oMessageView = new sap.m.MessageView({
					showDetailsPageHeader: false,
					itemSelect: function () {
						oBackButton.setVisible(true);
					},
					items: {
						path: "/aMockMessages",
						template: oMessageTemplate
					}
				}),
				oBackButton = new sap.m.Button({
					icon: sap.ui.core.IconPool.getIconURI("nav-back"),
					visible: false,
					press: function () {
						oMessageView.navigateBack();
						this.setVisible(false);
					}
				});

			oMessageView.setModel(oModel);

			var oCloseButton =  new sap.m.Button({
					text: "Close",
					press: function () {
						that._oPopover.close();
					}
				}),
				oPopoverFooter = new sap.m.Bar({
					contentRight: oCloseButton
				}),
				oPopoverBar = new sap.m.Bar({
					contentLeft: [oBackButton],
					contentMiddle: [
						new sap.ui.core.Icon({
							color: "#bb0000",
							src: "sap-icon://message-information"}),
						new sap.m.Text({
							text: "Messages"
						})
					]
				});

			this._oPopover = new sap.m.Popover({
				customHeader: oPopoverBar,
				contentWidth: "250px",
				contentHeight: "250px",
				verticalScrolling: false,
				modal: false,
				content: [oMessageView],
				footer: oPopoverFooter
			});
			pop=this._oPopover;
		},
		openDetail : function() {
							var newDetail = new TabContainerItem({
								name: 'Detail KPI',
								modified: false
							});	
							var tabContainer = this.byId("myTabContainer");

							tabContainer.addItem(
								newDetail
							);
							tabContainer.setSelectedItem(
								newDetail
							);		
			var oSideNavigation = this.byId('sideNavigation');
				oSideNavigation.setExpanded(false);
							
			
		},
		onAfterRendering : function(oEvent) {
			console.log('onAfterRendering');
			
		},		
		handlePopoverPress: function (oEvent) {
			this._oPopover.openBy(oEvent.getSource());
		},

		handlePressOpenMenu: function(oEvent) {
			var oSideNavigation = this.byId('sideNavigation');
			var bExpanded = oSideNavigation.getExpanded();

			oSideNavigation.setExpanded(!bExpanded);
		},

		handleMenuItemPress: function(oEvent) {
			var msg = "'" + oEvent.getParameter("item").getText() + "' pressed";
			MessageToast.show(msg);
		},

		handleTextFieldItemPress: function(oEvent) {
			var msg = "'" + oEvent.getParameter("item").getValue() + "' entered";
			MessageToast.show(msg);
		},
			itemCloseHandler: function(oEvent) {
				// prevent the tab being closed by default
				oEvent.preventDefault();

				var oTabContainer = this.byId("myTabContainer");
				var oItemToClose = oEvent.getParameter('item');

				MessageBox.confirm("Do you want to close the tab '" + oItemToClose.getName() + "'?", {
					onClose: function (oAction) {
						if (oAction === MessageBox.Action.OK) {
							oTabContainer.removeItem(oItemToClose);
							//MessageToast.show("Item closed: " + oItemToClose.getName(), {duration: 500});
						} else {
							//MessageToast.show("Item close canceled: " + oItemToClose.getName(), {duration: 500});
						}
					}
				});
			},
			exportPdf : function() {
				var doc = new jsPDF();
				 
				doc.setFontSize(30);
				 doc.text(50, 35, 'Products Detail ');
				 
				 
				doc.save('orderList.pdf');				
			},
			openTab : function(oControlEvent) {
			var oSideNavigation = this.byId('sideNavigation');
				oSideNavigation.setExpanded(false);
				var d = new Date();
				var n = d.getMilliseconds();
				var oFlexBoxDateTimePicker = new sap.m.FlexBox({
					width : "100%",	
					class : "columns",
					alignItems : "Start",
					justifyContent: "End",
                    maxContainerCols: 2,
                    items:[
						new sap.m.Button ({
							icon : 'sap-icon://pdf-attachment',
							press : function() {
								var doc = new jsPDF('landscape','mm','A2');
								var source = $('#content')[0];
								console.log(source);
								var options = {
													format: 'JPEG',
								//                    pagesplit: true,
													background: '#000'
												};
								
								doc.addHTML(source, function () {
									doc.save('kpiview.pdf');
								});
							}
						}),
						new sap.m.SegmentedButton({
							selectedKey: 'now',
							id: 'iSegment'+n,
							
						items: [
							new sap.m.SegmentedButtonItem({
								key: 'yesterday',
								width: 'auto',
								text: 'Gestern'
							}),
							new sap.m.SegmentedButtonItem({
								key: 'now',
								width: 'auto',
								text: 'Heute'
							})
							]
						}),
                        new sap.m.DateTimePicker({
							id: 'iDate'+n,
							valueFormat: "dd-MM-yyyy HH:mm:ss",
							value: new Date()


							})
                    ]
                });               			
				var that=this;
var kpi1= sap.ui.xmlfragment("idFragmentKpi1","kpiDetail.kpi1", this);
var kpi2= sap.ui.xmlfragment("idFragmentKpi2","kpiDetail.kpi2", this);			
var kpi3= sap.ui.xmlfragment("idFragmentKpi3","kpiDetail.kpi3", this);			
var kpi4= sap.ui.xmlfragment("idFragmentKpi4","kpiDetail.kpi4", this);			
var kpi5= sap.ui.xmlfragment("idFragmentKpi5","kpiDetail.kpi5", this);			
var kpi6= sap.ui.xmlfragment("idFragment6","kpiDetail.kpi1", this);			
var kpi7= sap.ui.xmlfragment("idFragment7","kpiDetail.kpi1", this);			
var kpi8= sap.ui.xmlfragment("idFragment8","kpiDetail.kpi1", this);			
var kpi9= sap.ui.xmlfragment("idFragment9","kpiDetail.kpi1", this);			
var kpi10= sap.ui.xmlfragment("idFragment10","kpiDetail.kpi1", this);			
var kpi11= sap.ui.xmlfragment("idFragment11","kpiDetail.kpi1", this);			
var kpi12= sap.ui.xmlfragment("idFragment12","kpiDetail.kpi1", this);			
				var oFlexBox = new sap.m.FlexBox({
					width : "100%",	
					class : "columns",
					alignItems : "Stretch",
                    maxContainerCols: 4,
                    items:[
                        kpi1.addStyleClass("column1"),
                        kpi2.addStyleClass("column1"),
                        kpi3.addStyleClass("column1"),
                        kpi4.addStyleClass("column1")
                    ]
                });               			
				var oFlexBox2 = new sap.m.FlexBox({
					width : "100%",	
					class : "columns",
					alignItems : "Stretch",
                    maxContainerCols: 4,
                    items:[
                        kpi5.addStyleClass("column1"),
                        kpi6.addStyleClass("column1"),
                        kpi7.addStyleClass("column1"),
                        kpi8.addStyleClass("column1")
                    ]
                });               			
				var oFlexBox3 = new sap.m.FlexBox({
					width : "100%",	
					class : "columns",
					alignItems : "Stretch",
                    maxContainerCols: 4,
                    items:[
                        kpi9.addStyleClass("column1"),
                        kpi10.addStyleClass("column1"),
                        kpi11.addStyleClass("column1"),
                        kpi12.addStyleClass("column1")
                    ]
                });               			
				console.log('opening tab');
				console.log(oControlEvent.getSource().mProperties.text);
				
				var newRole1 = new TabContainerItem({
					name: oControlEvent.getSource().mProperties.text,
					modified: false
				});
				newRole1.addContent(oFlexBoxDateTimePicker);
				newRole1.addContent(oFlexBox);
				newRole1.addContent(oFlexBox2);
				newRole1.addContent(oFlexBox3);
					//this.byId("iDate"+n).setDateValue(new Date());
				var tabContainer = this.byId("myTabContainer");

				tabContainer.addItem(
					newRole1
				);
				tabContainer.setSelectedItem(
					newRole1
				);
				kpi1.attachBrowserEvent("mouseover", function(oEvent) {
					pop.openBy(kpi1);
				});		
				kpi2.attachBrowserEvent("mouseover", function(oEvent) {
					pop.openBy(kpi2);
				});		
				kpi3.attachBrowserEvent("mouseover", function(oEvent) {
					pop.openBy(kpi3);
				});		
				kpi4.attachBrowserEvent("mouseover", function(oEvent) {
					pop.openBy(kpi4);
				});		
				kpi5.attachBrowserEvent("mouseover", function(oEvent) {
					pop.openBy(kpi5);
				});		
				kpi6.attachBrowserEvent("mouseover", function(oEvent) {
					pop.openBy(kpi6);
				});		
				kpi7.attachBrowserEvent("mouseover", function(oEvent) {
					pop.openBy(kpi7);
				});		
				kpi8.attachBrowserEvent("mouseover", function(oEvent) {
					pop.openBy(kpi8);
				});		
				kpi9.attachBrowserEvent("mouseover", function(oEvent) {
					pop.openBy(kpi9);
				});		
				kpi10.attachBrowserEvent("mouseover", function(oEvent) {
					pop.openBy(kpi10);
				});		
				kpi11.attachBrowserEvent("mouseover", function(oEvent) {
					pop.openBy(kpi11);
				});		
				kpi12.attachBrowserEvent("mouseover", function(oEvent) {
					pop.openBy(kpi12);
				});		
				
			}			
		

		

	});

	return MainController;

});
