var pop;
sap.ui.define([
		'sap/m/MessageToast',
		'sap/ui/core/Fragment',
		'sap/ui/core/mvc/Controller',
		'sap/m/TabContainerItem',
		'sap/m/MessageBox',
		'sap/m/FlexBox',
		'sap/ui/model/json/JSONModel'
		
	], function(MessageToast, Fragment, Controller,TabContainerItem, MessageBox,FlexBox,JSONModel) {
	"use strict";

	var MainController = Controller.extend("main.main", {

		drawKPI6: function() {
				var oModel=this.getView().getModel();
				var that=this;
				setTimeout(function() {			
			$.ajax
			({
			  type: "GET",
			  contentType : "application/x-www-form-urlencoded",
			  url: "https://taipscw4.w2:8080/sequenzerbestand?_max_rows=-1",
			  dataType: 'json',
			  async: true,
			 headers: {
								'contentType' : 'application/x-www-form-urlencoded',
								'Authorization' : 'Basic dGFpcHNjdzQ6dGFpcHNjdzQ='
								
							},  
			  success: function (theResponse){
				var sum=0;
				$.each( theResponse.collection, function( key, value ) {
					sum += parseInt(value.anzahl);
				});
				console.log(sum);
			  var dataKPI6= [{
                 "Monitor": "SF",
                 "Category1": sum				  
			  }]
						var getTimeString = function(input, separator) {
							var pad = function(input) {return input < 10 ? "0" + input : input;};
							var date = input ? new Date(input) : new Date();
							return [
								pad(date.getHours()),
								pad(date.getMinutes()),
								pad(date.getSeconds())
							].join(typeof separator !== 'undefined' ?  separator : ':' );
						}			
						oModel.setProperty("/timeKPI6", getTimeString());				
						oModel.setProperty("/dataKPI6", dataKPI6);
						oModel.refresh(true);
						$('#viz').empty();
						that.refreshDataSource('svg',1000,2000,1000,'SF');
						that.drawKPI6();
						}

			});		
		 },5000);			

		},
		drawKPI3: function() {
				var oModel=this.getView().getModel();
				var that=this;
				setTimeout(function() {			
			$.ajax
			({
			  type: "GET",
			  contentType : "application/x-www-form-urlencoded",
			  url: "https://taipscw4.w2:8080/sequenzerbestand?_max_rows=-1",
			  dataType: 'json',
			  async: true,
			 headers: {
								'contentType' : 'application/x-www-form-urlencoded',
								'Authorization' : 'Basic dGFpcHNjdzQ6dGFpcHNjdzQ='
								
							},  
			  success: function (theResponse){
				var sum=0;
				$.each( theResponse.collection, function( key, value ) {
					sum += parseInt(value.anzahl);
				});
			  var dataKPI6= [{
                 "Monitor": "SF",
                 "Category1": sum				  
			  }]
						var getTimeString = function(input, separator) {
							var pad = function(input) {return input < 10 ? "0" + input : input;};
							var date = input ? new Date(input) : new Date();
							return [
								pad(date.getHours()),
								pad(date.getMinutes()),
								pad(date.getSeconds())
							].join(typeof separator !== 'undefined' ?  separator : ':' );
						}			
						oModel.setProperty("/timeKPI3", getTimeString());				
						oModel.setProperty("/dataKPI3", dataKPI6);
						oModel.refresh(true);
						$('#vizkpi3').empty();
						that.refreshDataSource('svgkpi3',5,5,5,'VSF');
						that.drawKPI3();
						}

			});		
		 },5000);			

		},
		
		refreshDataSource : function(id,min1,min2,min3,name) {
			console.log('jdu kreslit');
    //Draw Stack Chart
    var marginStackChart = { top: 20, right: 40, bottom: 20, left: 40 },
            widthStackChart = 350 - marginStackChart.left - marginStackChart.right,
            heightStackChart = 169 - marginStackChart.top - marginStackChart.bottom;

    var xStackChart = d3.scaleBand()
            .range([0, widthStackChart])
            .padding(0.1);
    var yStackChart = d3.scaleLinear()
                .range([heightStackChart, 0]);


    var colorStackChart = d3.scaleOrdinal(["red", "orange", "green", "orange", "red"])
    var colorStackChart2 = d3.scaleOrdinal(["black","blue"])


    var canvasStackChart = d3.select("#"+id+"").append("svg")
        .attr("width", widthStackChart + marginStackChart.left + marginStackChart.right)
        .attr("height", heightStackChart + marginStackChart.top + marginStackChart.bottom)
        .append("g")
        .attr("transform", "translate(" + marginStackChart.left + "," + marginStackChart.top + ")");

   


        var data = [
             {
                 "Monitor": name,
                 "Category1": min1,
                 "Category2": min2,
                 "Category3": min3
             }
        ];
		var oModel=this.getView().getModel();
		
        var data2=oModel.getProperty("/dataKPI6");



            colorStackChart.domain(d3.keys(data[0]).filter(function (key) { return key !== "Monitor"; }));

            data.forEach(function (d) {
                var y0 = 0;
                d.ages = colorStackChart.domain().map(function (name) { return { name: name, y0: y0, y1: y0 += +d[name] }; });
                d.total = d.ages[d.ages.length - 1].y1;
            });

            data.sort(function (a, b) { return b.total - a.total; });

            xStackChart.domain(data.map(function (d) { return d.Monitor; }));
            yStackChart.domain([0, d3.max(data, function (d) { return d.total; })]);

            canvasStackChart.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + heightStackChart + ")")
            .call(d3.axisBottom(xStackChart));

            canvasStackChart.append("g")
            .attr("class", "y axis")
            .call(d3.axisLeft(yStackChart))
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text("No Of Buildings");

            var state = canvasStackChart.selectAll(".Monitor")
            .data(data)
            .enter().append("g")
            .attr("class", "g")
            .attr("transform", function (d) { return "translate(" + xStackChart(d.Monitor) + ",0)"; });

            state.selectAll("rect")
            .data(function (d) { return d.ages; })
            .enter().append("rect")
            .attr("width", xStackChart.bandwidth())
            .attr("y", function (d) { return yStackChart(d.y1); })
            .attr("height", function (d) { return yStackChart(d.y0) - yStackChart(d.y1); })
            .style("fill", function (d) { return colorStackChart(d.name); });

			//adding black stuff
			
            colorStackChart.domain(d3.keys(data2[0]).filter(function (key) { return key !== "Monitor"; }));

            data2.forEach(function (d) {
                var y0 = 0;
                d.ages = colorStackChart.domain().map(function (name) { return { name: name, y0: y0, y1: y0 += +d[name] }; });
                d.total = d.ages[d.ages.length - 1].y1;
            });

            data2.sort(function (a, b) { return b.total - a.total; });

            xStackChart.domain(data2.map(function (d) { return d.Monitor; }));
            yStackChart.domain([0, d3.max(data, function (d) { return d.total; })]);


            var state = canvasStackChart.selectAll(".Monitor")
            .data(data2)
            .enter().append("g")
            .attr("class", "g")
            .attr("transform", function (d) { return "translate(" + xStackChart(d.Monitor) + ",0)"; });

            state.selectAll("rect")
            .data(function (d) { return d.ages; })
            .enter().append("rect")
            .attr("width", xStackChart.bandwidth()/4)
            .attr("x", function (d) { return xStackChart.bandwidth()/4+xStackChart.bandwidth()/8; })
            .attr("y", function (d) { return yStackChart(d.y1); })
            .attr("height", function (d) { return yStackChart(d.y0) - yStackChart(d.y1); })
            .style("fill", function (d) { return colorStackChart2(d.name); });
			
	            var state2 = canvasStackChart.selectAll(".Monitor")
            .data(data2)
            .enter().append("g")
            .attr("class", "g")
            .attr("transform", function (d) { return "translate(" + xStackChart(d.Monitor) + ",0)"; });		
			
state2.selectAll("rect")
  .data(function (d) { return d.ages; })
	.enter().append("text")
  .attr("class", "bar")
  .style("fill", "#ffffff")
  .attr("text-anchor", "middle")
  .attr("x", function(d) { return xStackChart.bandwidth()/2})
  .attr("y", function(d) { return yStackChart(d.y0) - yStackChart(d.y1) +20; })
  .text(function(d) {  return d.y1; });
		},
		
		refresh : function() {
				var oModel=this.getView().getModel();
				var that=this;
				setTimeout(function() {
						var getTimeString = function(input, separator) {
							var pad = function(input) {return input < 10 ? "0" + input : input;};
							var date = input ? new Date(input) : new Date();
							return [
								pad(date.getHours()),
								pad(date.getMinutes()),
								pad(date.getSeconds())
							].join(typeof separator !== 'undefined' ?  separator : ':' );
						}			
						oModel.setProperty("/time", getTimeString());
						oModel.refresh(true);
						
						that.refresh();
					},1000);			
			
		},

		onInit: function(){
			var oMenuButton = this.byId('openMenu');
			var thar=this;
			console.log('Button: '+oMenuButton);
			oMenuButton.attachBrowserEvent("mouseover", function(oEvent) {
				var oSideNavigation = that.byId('sideNavigation');
				oSideNavigation.setExpanded(true);
			});			
			var oSideNavigation = this.byId('sideNavigation');
			console.log(oSideNavigation);
			console.log(oSideNavigation.getItem().mAggregations.items[0].sId);
			var first = this.byId(oSideNavigation.getItem().mAggregations.items[0].sId);
			var oModel = new JSONModel();
			var getTimeString = function(input, separator) {
				var pad = function(input) {return input < 10 ? "0" + input : input;};
				var date = input ? new Date(input) : new Date();
				return [
					pad(date.getHours()),
					pad(date.getMinutes()),
					pad(date.getSeconds())
				].join(typeof separator !== 'undefined' ?  separator : ':' );
			}			
			var data = {
					now : new Date(),
					time: getTimeString(),
					timeKPI6: getTimeString(),
dataKPI6 : [
             {
                 "Monitor": "SF",
                 "Category1": "900"
             }],					
aMockMessages : [{
				type: 'Warning',
				title: 'Information message',
				description: 'First Information message description',
				subtitle: 'Example of subtitle',
				counter: 1
			}]					
			};
    
			oModel.setData(data);
			var that = this;
			this.getView().setModel(oModel);
			this.refresh();			

			var oMessageTemplate = new sap.m.MessageItem({
				type: '{type}',
				title: '{title}',
				description: '{description}',
				subtitle: '{subtitle}',
				counter: '{counter}',
				markupDescription: "{markupDescription}"
			});


				that = this;
			

			var oMessageView = new sap.m.MessageView({
					showDetailsPageHeader: false,
					itemSelect: function () {
						oBackButton.setVisible(true);
					},
					items: {
						path: "/aMockMessages",
						template: oMessageTemplate
					}
				}),
				oBackButton = new sap.m.Button({
					icon: sap.ui.core.IconPool.getIconURI("nav-back"),
					visible: false,
					press: function () {
						oMessageView.navigateBack();
						this.setVisible(false);
					}
				});

			oMessageView.setModel(oModel);

			var oCloseButton =  new sap.m.Button({
					text: "Close",
					press: function () {
						that._oPopover.close();
					}
				}),
				oPopoverFooter = new sap.m.Bar({
					contentRight: oCloseButton
				}),
				oPopoverBar = new sap.m.Bar({
					contentLeft: [oBackButton],
					contentMiddle: [
						new sap.ui.core.Icon({
							color: "#bb0000",
							src: "sap-icon://message-information"}),
						new sap.m.Text({
							text: "Messages"
						})
					]
				});

			this._oPopover = new sap.m.Popover({
				customHeader: oPopoverBar,
				contentWidth: "250px",
				contentHeight: "250px",
				verticalScrolling: false,
				modal: false,
				placement: "HorizontalPreferredRight",
				content: [oMessageView],
				footer: oPopoverFooter
			});
			pop=this._oPopover;
			this.openTab();
		},
		openDetail : function() {
							var newDetail = new TabContainerItem({
								name: 'Detail KPI',
								modified: false
							});	
							var tabContainer = this.byId("myTabContainer");

							tabContainer.addItem(
								newDetail
							);
							tabContainer.setSelectedItem(
								newDetail
							);		
			var oSideNavigation = this.byId('sideNavigation');
				oSideNavigation.setExpanded(false);
							
			
		},
		onAfterRendering : function(oEvent) {
			console.log('onAfterRendering');
			
		},		
		handlePopoverPress: function (oEvent) {
			this._oPopover.openBy(oEvent.getSource());
		},

		handlePressOpenMenu: function(oEvent) {
			var oSideNavigation = this.byId('sideNavigation');
			var bExpanded = oSideNavigation.getExpanded();

			oSideNavigation.setExpanded(!bExpanded);
		},

		handleMenuItemPress: function(oEvent) {
			var msg = "'" + oEvent.getParameter("item").getText() + "' pressed";
			MessageToast.show(msg);
		},

		handleTextFieldItemPress: function(oEvent) {
			var msg = "'" + oEvent.getParameter("item").getValue() + "' entered";
			MessageToast.show(msg);
		},
			itemCloseHandler: function(oEvent) {
				// prevent the tab being closed by default
				oEvent.preventDefault();

				var oTabContainer = this.byId("myTabContainer");
				var oItemToClose = oEvent.getParameter('item');

				MessageBox.confirm("Do you want to close the tab '" + oItemToClose.getName() + "'?", {
					onClose: function (oAction) {
						if (oAction === MessageBox.Action.OK) {
							oTabContainer.removeItem(oItemToClose);
							//MessageToast.show("Item closed: " + oItemToClose.getName(), {duration: 500});
						} else {
							//MessageToast.show("Item close canceled: " + oItemToClose.getName(), {duration: 500});
						}
					}
				});
			},
			exportPdf : function() {
				var doc = new jsPDF();
				 
				doc.setFontSize(30);
				 doc.text(50, 35, 'Products Detail ');
				 
				 
				doc.save('orderList.pdf');				
			},
			openTab : function(oControlEvent) {
				console.log(oControlEvent);
				var oSideNavigation = this.byId('sideNavigation');
				oSideNavigation.setExpanded(false);
				var d = new Date();
				var n = d.getMilliseconds();
				var oFlexBoxDateTimePicker = new sap.m.FlexBox({
					width : "100%",	
					class : "columns",
					alignItems : "Start",
					justifyContent: "End",
                    maxContainerCols: 2,
                    items:[
						new sap.m.Button ({
							icon : 'sap-icon://pdf-attachment',
							press : function() {
								var doc = new jsPDF('landscape','mm','A2');
								var source = $('#content')[0];
								console.log(source);
								var options = {
													format: 'JPEG',
								//                    pagesplit: true,
													background: '#000'
												};
								
								doc.addHTML(source, function () {
									doc.save('kpiview.pdf');
								});
							}
						}),
						new sap.m.SegmentedButton({
							selectedKey: 'now',
							id: 'iSegment'+n,
							
						items: [
							new sap.m.SegmentedButtonItem({
								key: 'yesterday',
								width: 'auto',
								text: 'Gestern'
							}),
							new sap.m.SegmentedButtonItem({
								key: 'now',
								width: 'auto',
								text: 'Heute'
							})
							]
						}),
                        new sap.m.DateTimePicker({
							id: 'iDate'+n,
							valueFormat: "dd.MM.yyyy, HH:mm:ss",
							value: [{
								path: "/data/now",
								type:'sap.ui.model.type.DateTime',
								formatOptions: [{ style: 'medium', strictParsing: false}]
							}]


							}),
						new sap.m.Button ({
							icon : 'sap-icon://bell',
							press : function() {
							}
						})							
                    ]
                });   
				
				var that=this;
var kpi1= sap.ui.xmlfragment("idFragmentKpi1","kpiDetail.kpi1", this);
var kpi2= sap.ui.xmlfragment("idFragmentKpi2","kpiDetail.kpi2", this);			
var kpi3= sap.ui.xmlfragment("idFragmentKpi3","kpiDetail.kpi3", this);			
var kpi4= sap.ui.xmlfragment("idFragmentKpi4","kpiDetail.kpi4", this);			
var kpi5= sap.ui.xmlfragment("idFragmentKpi5","kpiDetail.kpi5", this);			
var kpi6= sap.ui.xmlfragment("idFragmentKpi6","kpiDetail.kpi6", this);			
var kpi7= sap.ui.xmlfragment("idFragmentKpi7","kpiDetail.kpi7", this);			
var kpi8= sap.ui.xmlfragment("idFragmentKpi8","kpiDetail.kpi8", this);			
var kpi9= sap.ui.xmlfragment("idFragmentKpi9","kpiDetail.kpi9", this);			
var kpi10= sap.ui.xmlfragment("idFragmentKpi10","kpiDetail.kpi10", this);			
var kpi11= sap.ui.xmlfragment("idFragmentKpi11","kpiDetail.kpi11", this);			
var kpi12= sap.ui.xmlfragment("idFragmentKpi12","kpiDetail.kpi12", this);			
				var oFlexBox = new sap.m.FlexBox({
					width : "100%",	
					class : "columns",
					alignItems : "Stretch",
                    maxContainerCols: 4,
                    items:[
                        kpi1.addStyleClass("column1"),
                        kpi2.addStyleClass("column1"),
                        kpi3.addStyleClass("column1"),
                        kpi4.addStyleClass("column1")
                    ]
                });               			
				var oFlexBox2 = new sap.m.FlexBox({
					width : "100%",	
					class : "columns",
					alignItems : "Stretch",
                    maxContainerCols: 4,
                    items:[
                        kpi5.addStyleClass("column1"),
                        kpi6.addStyleClass("column1"),
                        kpi7.addStyleClass("column1"),
                        kpi8.addStyleClass("column1")
                    ]
                });               			
				var oFlexBox3 = new sap.m.FlexBox({
					width : "100%",	
					class : "columns",
					alignItems : "Stretch",
                    maxContainerCols: 4,
                    items:[
                        kpi9.addStyleClass("column1"),
                        kpi10.addStyleClass("column1"),
                        kpi11.addStyleClass("column1"),
                        kpi12.addStyleClass("column1")
                    ]
                });               			
				console.log('opening tab');
				try {
					var tHeading=oControlEvent.getSource().mProperties.text;
				} catch(e) {
					var tHeading="Fertigungssteuerung (übergreifend)";
				}
				var newRole1 = new TabContainerItem({
					name: tHeading,
					modified: false
				});
				newRole1.addContent(oFlexBoxDateTimePicker);
				newRole1.addContent(oFlexBox);
				newRole1.addContent(oFlexBox2);
				newRole1.addContent(oFlexBox3);
					//this.byId("iDate"+n).setDateValue(new Date());
				var tabContainer = this.byId("myTabContainer");

				tabContainer.addItem(
					newRole1
				);
				tabContainer.setSelectedItem(
					newRole1
				);
				var setTimeoutConst;
				kpi1.attachBrowserEvent("mouseover", function(oEvent) {
					setTimeoutConst = setTimeout(function() {
						console.log('hover');
						pop.openBy(kpi1);
					},1250);
				});		
				kpi1.attachBrowserEvent("mouseout", function(oEvent) {
						clearTimeout(setTimeoutConst);
						pop.close();
				});		
				kpi2.attachBrowserEvent("mouseover", function(oEvent) {
					setTimeoutConst = setTimeout(function() {
						console.log('hover');
						pop.openBy(kpi2);
					},1250);
				});		
				kpi2.attachBrowserEvent("mouseout", function(oEvent) {
						clearTimeout(setTimeoutConst);
						pop.close();
				});		
				kpi3.attachBrowserEvent("mouseover", function(oEvent) {
					setTimeoutConst = setTimeout(function() {
						console.log('hover');
						pop.openBy(kpi3);
					},1250);
				});		
				kpi3.attachBrowserEvent("mouseout", function(oEvent) {
						clearTimeout(setTimeoutConst);
						pop.close();
				});		
				kpi4.attachBrowserEvent("mouseover", function(oEvent) {
					setTimeoutConst = setTimeout(function() {
						console.log('hover');
						pop.openBy(kpi4);
					},1250);
				});		
				kpi4.attachBrowserEvent("mouseout", function(oEvent) {
						clearTimeout(setTimeoutConst);
						pop.close();
				});		
				kpi5.attachBrowserEvent("mouseover", function(oEvent) {
					setTimeoutConst = setTimeout(function() {
						console.log('hover');
						pop.openBy(kpi5);
					},1250);
				});		
				kpi5.attachBrowserEvent("mouseout", function(oEvent) {
						clearTimeout(setTimeoutConst);
						pop.close();
				});		
				kpi6.attachBrowserEvent("mouseover", function(oEvent) {
					setTimeoutConst = setTimeout(function() {
						console.log('hover');
						pop.openBy(kpi6);
					},1250);
				});		
				kpi6.attachBrowserEvent("mouseout", function(oEvent) {
						clearTimeout(setTimeoutConst);
						pop.close();
				});		
				kpi7.attachBrowserEvent("mouseover", function(oEvent) {
					setTimeoutConst = setTimeout(function() {
						console.log('hover');
						pop.openBy(kpi7);
					},1250);
				});		
				kpi7.attachBrowserEvent("mouseout", function(oEvent) {
						clearTimeout(setTimeoutConst);
						pop.close();
				});		
				kpi8.attachBrowserEvent("mouseover", function(oEvent) {
					setTimeoutConst = setTimeout(function() {
						console.log('hover');
						pop.openBy(kpi8);
					},1250);
				});		
				kpi8.attachBrowserEvent("mouseout", function(oEvent) {
						clearTimeout(setTimeoutConst);
						pop.close();
				});		
				kpi9.attachBrowserEvent("mouseover", function(oEvent) {
					setTimeoutConst = setTimeout(function() {
						console.log('hover');
						pop.openBy(kpi9);
					},1250);
				});		
				kpi9.attachBrowserEvent("mouseout", function(oEvent) {
						clearTimeout(setTimeoutConst);
						pop.close();
				});		
				kpi10.attachBrowserEvent("mouseover", function(oEvent) {
					setTimeoutConst = setTimeout(function() {
						console.log('hover');
						pop.openBy(kpi10);
					},1250);
				});		
				kpi10.attachBrowserEvent("mouseout", function(oEvent) {
						clearTimeout(setTimeoutConst);
						pop.close();
				});		
				kpi11.attachBrowserEvent("mouseover", function(oEvent) {
					setTimeoutConst = setTimeout(function() {
						console.log('hover');
						pop.openBy(kpi11);
					},1250);
				});		
				kpi11.attachBrowserEvent("mouseout", function(oEvent) {
						clearTimeout(setTimeoutConst);
						pop.close();
				});		
				kpi12.attachBrowserEvent("mouseover", function(oEvent) {
					setTimeoutConst = setTimeout(function() {
						console.log('hover');
						pop.openBy(kpi12);
					},1250);
				});		
				kpi12.attachBrowserEvent("mouseout", function(oEvent) {
						clearTimeout(setTimeoutConst);
						pop.close();
				});		
			}			
		

		

	});

	return MainController;

});
